
from mpi4py import MPI
from sys import argv
from math  import sqrt
import operator
import time

ZASIEG_JANKIELA = 3.0
CZAS_GRANIA = 2

TWOJA_KOLEJ = "TWOJA_KOLEJ"    #Jankiel wysylajacy ta wiadomosc, raczej nie zagra w tej rundzie
MOJA_KOLEJ = "MOJA_KOLEJ"      #Jankiel wysylajacy ta wiadomosc, chce grac w tej rundzie
NIE_WIEM = "NIEWIEM"       #Jankiel jeszcze nie wie
GRAM = "GRAM"
NIE_GRAM = "NIE_GRAM"
KONIEC_GRANIA = "KONIEC"


skrypt, plik, n = argv
n = int(n)
class Lokalizacja(object):
  def __init__(self,x,y):
    self.x = x
    self.y = y
  def Dystans(self,other):
    dx = other.x - self.x
    dy = other.y - self.y
    return sqrt(dx*dx + dy*dy)


def pogadaj_z_sasiadami(wiadomosci,odpowiedz = {}):
    for sasiad in filter(lambda x: x<rank,sasiedzi):
        st = comm.recv(source=sasiad)
        comm.send(wiadomosci[sasiad],dest=sasiad)
        odpowiedz[sasiad]=st
    for sasiad in filter(lambda x: x>rank,sasiedzi):
        comm.send(wiadomosci[sasiad],dest=sasiad)
        st = comm.recv(source=sasiad)
        odpowiedz[sasiad]=st

def gen_wiad(stopienJankiela,stopienSasiada):
    if stopienJankiela == stopienSasiada:
        return NIE_WIEM
    elif stopienJankiela < stopienSasiada:
        return TWOJA_KOLEJ
    else:
        return MOJA_KOLEJ
def granie(rank):
    time.sleep(CZAS_GRANIA);
    print ">>>Jankiel-{0} GRA!".format(rank)

#algorytm dla kazdego Jankiela
#0. Jesli nie masz sasiadow to graj i zakoncz
#1. Rozeslij swoj stopien ( liczbe sasiadow)
#2. Dla sasiadow o mniejszym stopniu wyslij MOJA_KOLEJ, o wiekszym TWOJA_KOLEJ, o tym samym NIE_WIEM
#3. Jesli otrzymales:
#   a) same TWOJA_KOLEJ, rozeslij MOJA KOLEJ,
#   b) TWOJA_KOLEJ, lub NIE_WIEM to jesli jestes Jankielem o nizszym numerze to MOJA_KOLEJ, jesli o wyzszym numerze to TWOJA_KOLEJ
#   c) w p.p. TWOJA_KOLEJ
#4. Jesli otrzymales same TWOJA_KOLEJ to rozeslij GRAM, w p.p. NIE_GRAM
#5. Jesli grasz to:
#   a) zagraj (2sek.)
#   b) rozeslij KONIEC do niegrajacych sasiadow
#   c) zakoncz
#6. Jesli nie grasz to:
#   a) czekaj na koniec grania swoich sasiadow
#   b) usun grajacych sasiadow
#7. Wroc do 0

comm = MPI.COMM_WORLD
rank = comm.Get_rank()



rozm = open(plik)
if int(rozm.readline()) != n :
    print "Zle podano n"
    exit()
pozostaliJankiele = {}
sasiedzi = []
mojaLokalizacja = Lokalizacja(0,0)
for i in range(0, n):
    xx, yy = rozm.readline().split()
    tmp = Lokalizacja( int(xx), int(yy) )
    if i == rank:
        mojaLokalizacja = tmp
        print "Jestem Jankiel-{0} moja lokalizacja to: {1},{2}".format( rank, mojaLokalizacja.x, mojaLokalizacja.y )
    else:
        pozostaliJankiele[i] = tmp;

for k,v in pozostaliJankiele.iteritems():
    if v.Dystans(mojaLokalizacja) < ZASIEG_JANKIELA:
        sasiedzi.append(k)

print "Jankiele w zasiegu Jankiela-{0} to: {1}".format(rank,sasiedzi)


sasiedzi.sort()
 
while 1:
    
    stopien = len(sasiedzi)
    if stopien == 0:
        granie(rank)
        exit()
    stopnieSasiad={}
    #comm.Barrier()

    #1. Wymiana swoich stopni
    wiadomosc_stopien =  dict(zip(sasiedzi,[stopien]*stopien))
    pogadaj_z_sasiadami(wiadomosc_stopien,stopnieSasiad)
    print "Jankiele w zasiegu Jankiela-{0} maja nastepujaca ilosc sasiadow: {1}".format(rank,stopnieSasiad)
    stopnieSasiad = dict(sorted(stopnieSasiad.items(),key=operator.itemgetter(1)))

    #2. przygotowanie wiadomosci
    maxim = max(stopnieSasiad.iteritems(),key=operator.itemgetter(1))[1]
    if stopien > maxim:
        wiad = dict(zip(sasiedzi,[MOJA_KOLEJ]*stopien))
    else:
        print "Jankiel-",rank,": Moj stopien: ",stopien," najwiekszy sasiad: ",maxim
        wiad = dict(map(lambda (k,v): (k,gen_wiad(stopien,v)), stopnieSasiad.iteritems()))

    odebr = {}
    pogadaj_z_sasiadami(wiad,odebr)
    print ">Jankiel-{0} wstepnie otrzymal {1}".format(rank,odebr)
    #3. po odebraniu wstepnych preferencji grania, potwierdzenie ich
    if all(val == TWOJA_KOLEJ for val in odebr.values()):
        moja_wiadomosc = MOJA_KOLEJ    
    elif all((val == TWOJA_KOLEJ or val == NIE_WIEM) for val in odebr.values()):
        najw_sasiad = max(stopnieSasiad.iteritems(),key=operator.itemgetter(1))[0]
        if najw_sasiad < rank:
             moja_wiadomosc = TWOJA_KOLEJ
        else:
             moja_wiadomosc = MOJA_KOLEJ
    else:
          moja_wiadomosc = TWOJA_KOLEJ

    wiad = dict(zip(sasiedzi,[moja_wiadomosc]*stopien))
    pogadaj_z_sasiadami(wiad,odebr)

    #4. wyslanie ostatecznej decyzji    
    print ">>Jankiel-{0} osstatecznie otrzymal {1}".format(rank,odebr)
    
    if all(val == TWOJA_KOLEJ for val in odebr.values()):
        moja_wiadomosc = GRAM
    else:
        moja_wiadomosc = NIE_GRAM

    wiad = dict(zip(sasiedzi,[moja_wiadomosc]*stopien))
    pogadaj_z_sasiadami(wiad,odebr)
    print ">>Jankiela-{0} ktorzy graja to {1}".format(rank,odebr)

    

    if moja_wiadomosc == GRAM :
        granie(rank)
        for sasiad in sasiedzi:
            comm.send(KONIEC_GRANIA,dest=sasiad)
        exit()
        print "Jankiel-{0} konczy".format(rank)
    else:
        for sasiad, czy_gra in odebr.iteritems():
            if czy_gra == GRAM:
                print "Jankiel-{0} czeka na {1}".format(rank,sasiad)
                comm.recv(source=sasiad);
                sasiedzi.remove(sasiad);




            
            

    

    




print 'Otrzymane wiadomosci - Jankiel-',rank,': ',odebr


# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
